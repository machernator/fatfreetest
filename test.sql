
UPDATE courses SET  title='the first course',
                     description='a wonderful description',
                     date_start='2023-03-30',
                     date_end='2023-04-01',
                     max_students=200,
                     active=1
				WHERE course_id=1