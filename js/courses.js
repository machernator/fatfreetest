function onCourseClicked(e) {
	/*
	e.target -> geklicktes Element, e.currentTarget & this -> Besitzer des Events
	console.log(e.target, e.currentTarget, this);
	 */
	/*
		Bei verschachtelten Elementen kann es sein, dass das gesuchte Element ein Elternelement ist.
		element.closest hilft uns beim identifizieren:
		Bist du .btn-delete oder wurde ein Kind Element des .btn-delete geklickt
	*/
	if (e.target.closest('.btn-delete')) {
		// cid aus data-cid lesen - dataset.nameDesAttributs. data-attributes die mit - geschrieben werden,
		// werden in camelCase ausgelesen: data-dies-und-das element.dataset.diesUndDas

		deleteCourse(e.target.closest('.btn-delete'));
	}

}

/**
 * Lösche aus DB, lösche bei Erfolg die geklickte Zeile aus der Tabelle
 *
 * @param   Element  el  [el description]
 *
 * @return void
 */
function deleteCourse(el) {
	const cid = el.dataset.cid;
	const data = { "cid": cid };
	fetch('/course', {
		'method': 'DELETE',
		// im Body Daten als query String anfügen name values pairs
		'body': 'cid=' + cid
	})
		// Antwort String in JSON Objekt umwandeln
		.then((response) => response.json())
		// JSON Objekt (data) verarbeiten
		.then((data) => {
			// trat kein Fehler auf, löschen wir die Zeile
			if (data.error === null) {
				const tr = el.closest('tr');
				tr.remove();
			}
		});
}

// Table selektieren
const coursesTable = document.getElementById('coursesTable');

coursesTable.addEventListener('pointerup', onCourseClicked);
