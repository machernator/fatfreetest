function onDeleteCourse(e) {
	console.log(e);
}

// Elemente selektieren
const btnDelete = document.querySelectorAll('.btn-delete');
// const btnDelete = document.getElementsByClassName('btn-delete')

// Event Handler zuweisen
/*
	Beachte: wenn eine benannte function als callback übergeben wird, wird NUR
	der Name der Funktion geschrieben. Funktionen agieren wie Variablen (Name der Funktion
	ist wie ein Variablen Name). Typische Fehler
	Funktion aufrufen (durch die ())
	btnDelete.forEach(el => el.addEventListener('pointerup', onDeleteCourse())

	Funktion als String übergeben
	btnDelete.forEach(el => el.addEventListener('pointerup', 'onDeleteCourse')

	Hier: 160 Event Handler erzeugt
*/
btnDelete.forEach(el => el.addEventListener('pointerup', onDeleteCourse));

// Event functions schreiben siehe oben