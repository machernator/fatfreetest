/* Alle orders */
SELECT * FROM `orders`

/* WHERE schränkt die Anzahl der gefundenen Felder ein */
SELECT * FROM `orders` WHERE `orderNumber` = 10100;

/*
	Eine Rechnung selektieren und mit dem entsprechenden Kunden verknüpfen.
	Die Daten beider werden in einer Zeile ausgegeben
 */
SELECT * FROM orders
LEFT JOIN customers
ON orders.customerNumber = customers.customerNumber
WHERE orders.orderNumber = 10100;

/* Alle selektieren */
SELECT * FROM orders
LEFT JOIN customers
ON orders.customerNumber = customers.customerNumber

/* Nur bestimmte Felder selektieren */
SELECT
orders.orderDate, orders.shippedDate, orders.status,
customers.customerName, customers.phone
FROM orders
LEFT JOIN customers
ON orders.customerNumber = customers.customerNumber
WHERE orders.orderNumber = 10100;

/* Zahlungen für kunden 112 auswählen */
SELECT payments.*, customers.customerName, customers.city
FROM payments
LEFT JOIN customers
ON payments.customerNumber = customers.customerNumber
WHERE payments.customerNumber = 112;

/* Alternative Schreibweise für Inner Join, bitte nicht verwenden */
SELECT payments.*, customers.customerName, customers.city
FROM payments, customers
WHERE payments.customerNumber = 112
AND payments.customerNumber = customers.customerNumber;