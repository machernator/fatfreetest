-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Erstellungszeit: 23. Mrz 2023 um 10:11
-- Server-Version: 10.8.3-MariaDB
-- PHP-Version: 8.0.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `institut`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `courses`
--

CREATE TABLE `courses` (
  `course_id` int(11) NOT NULL,
  `room_id` int(11) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `description` text DEFAULT NULL,
  `date_start` date NOT NULL,
  `date_end` date DEFAULT NULL,
  `max_students` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `courses`
--

INSERT INTO `courses` (`course_id`, `room_id`, `title`, `description`, `date_start`, `date_end`, `max_students`, `active`) VALUES
(1, NULL, 'the first course', 'a wonderful description', '2023-03-30', '2023-04-01', 200, 1),
(2, NULL, 'Accounting Assistant I', 'repurpose granular portals', '2024-06-23', NULL, 18, 0),
(3, NULL, 'Data Coordiator', 'iterate frictionless initiatives', '2022-07-01', NULL, 5, 1),
(4, NULL, 'VP Product Management', 'engineer magnetic solutions', '2023-06-04', NULL, 13, 0),
(5, NULL, 'Human Resources Manager', 'orchestrate dot-com schemas', '2023-03-16', '2023-01-30', 12, 1),
(6, NULL, 'Staff Accountant II', 'monetize viral architectures', '2023-11-08', NULL, 19, 1),
(7, NULL, 'Sales Associate', 'e-enable B2B experiences', '2023-03-03', '2022-02-23', 7, 1),
(8, NULL, 'Technical Writer', 'revolutionize bricks-and-clicks technologies', '2022-09-05', NULL, 16, 0),
(9, NULL, 'Statistician II', 'deploy next-generation e-tailers', '2022-11-02', NULL, 15, 0),
(10, NULL, 'Computer Systems Analyst III', 'innovate enterprise e-business', '2023-07-16', '2023-01-24', 14, 1),
(11, NULL, 'Nuclear Power Engineer', 'synergize frictionless eyeballs', '2022-06-23', NULL, 14, 0),
(12, NULL, 'Media Manager I', 'disintermediate B2B vortals', '2023-03-04', NULL, 13, 1),
(13, NULL, 'Recruiter', 'target viral e-commerce', '2023-03-18', NULL, 19, 1),
(14, NULL, 'Senior Cost Accountant', 'maximize value-added communities', '2022-12-09', NULL, 2, 1),
(15, NULL, 'Civil Engineer', 'maximize killer infrastructures', '2022-10-06', '2023-02-27', 15, 0),
(16, NULL, 'Registered Nurse', 'envisioneer distributed interfaces', '2024-01-28', NULL, 11, 1),
(17, NULL, 'Pharmacist', 'transform integrated platforms', '2022-09-27', NULL, 10, 0),
(18, NULL, 'Senior Editor', 'aggregate wireless communities', '2023-11-09', NULL, 20, 0),
(19, NULL, 'Physical Therapy Assistant', 'evolve proactive niches', '2023-07-29', NULL, 20, 0),
(20, NULL, 'Dental Hygienist', 'embrace bleeding-edge platforms', '2023-06-12', NULL, 2, 0),
(21, NULL, 'Research Assistant II', 'unleash seamless interfaces', '2023-12-18', NULL, 10, 1),
(22, NULL, 'Sales Representative', 'brand distributed initiatives', '2023-01-06', NULL, 6, 1),
(23, NULL, 'Clinical Specialist', 'synthesize ubiquitous metrics', '2023-06-22', '2024-02-23', 14, 1),
(24, NULL, 'Software Test Engineer I', 'reinvent dot-com e-markets', '2023-03-26', '2023-01-14', 5, 0),
(26, NULL, 'Staff Scientist', 'optimize interactive e-services', '2023-09-04', NULL, 17, 0),
(27, NULL, 'Electrical Engineer', 'innovate holistic relationships', '2023-01-25', '2022-09-20', 15, 1),
(28, NULL, 'Executive Secretary', 'incentivize best-of-breed ROI', '2023-11-01', NULL, 17, 1),
(29, NULL, 'Recruiter', 'synergize seamless architectures', '2023-08-27', NULL, 14, 1),
(30, NULL, 'Chief Design Engineer', 'e-enable cutting-edge functionalities', '2022-11-26', NULL, 8, 0),
(31, NULL, 'Marketing Assistant', 'exploit frictionless e-tailers', '2023-07-01', NULL, 2, 0),
(32, NULL, 'Environmental Tech', 'deliver bricks-and-clicks communities', '2024-02-24', NULL, 19, 1),
(33, NULL, 'Help Desk Operator', 'mesh magnetic web-readiness', '2023-03-11', '2024-01-05', 9, 1),
(34, NULL, 'Quality Control Specialist', 'evolve back-end synergies', '2023-11-18', NULL, 12, 0),
(35, NULL, 'Senior Quality Engineer', 'visualize cutting-edge relationships', '2022-06-13', '2022-06-29', 19, 0),
(36, NULL, 'Cost Accountant', 'integrate visionary infomediaries', '2022-06-30', '2024-04-06', 9, 1),
(37, NULL, 'Civil Engineer', 'orchestrate B2C content', '2024-05-27', NULL, 13, 1),
(38, NULL, 'Senior Cost Accountant', 'seize end-to-end e-markets', '2023-02-05', NULL, 15, 1),
(39, NULL, 'VP Marketing', 'harness distributed niches', '2023-09-08', NULL, 10, 0),
(40, NULL, 'VP Sales', 'strategize distributed functionalities', '2023-06-27', NULL, 14, 0),
(41, NULL, 'Human Resources Manager', 'scale extensible web-readiness', '2024-05-02', NULL, 9, 0),
(42, NULL, 'Information Systems Manager', 'matrix viral channels', '2023-07-22', '2023-09-16', 6, 0),
(43, NULL, 'General Manager', 'integrate real-time action-items', '2022-02-28', '2023-03-20', 7, 0),
(44, NULL, 'Senior Financial Analyst', 'extend dot-com channels', '2023-10-21', NULL, 13, 0),
(45, NULL, 'Senior Financial Analyst', 'monetize B2B web-readiness', '2024-01-11', '2023-06-13', 6, 1),
(46, NULL, 'Compensation Analyst', 'integrate scalable eyeballs', '2022-04-28', NULL, 19, 1),
(47, NULL, 'Structural Analysis Engineer', 'benchmark magnetic deliverables', '2022-11-02', '2023-09-27', 3, 1),
(48, NULL, 'Programmer II', 'implement world-class synergies', '2022-11-24', NULL, 7, 0),
(49, NULL, 'Staff Scientist', 'drive 24/7 portals', '2024-05-29', NULL, 8, 1),
(50, NULL, 'Nurse Practicioner', 'scale interactive ROI', '2023-09-22', '2022-03-09', 20, 1),
(51, NULL, 'VP Quality Control', 'grow customized functionalities', '2023-01-24', NULL, 7, 1),
(52, NULL, 'Senior Quality Engineer', 'leverage scalable partnerships', '2023-11-08', '2023-02-05', 15, 1),
(53, NULL, 'Marketing Manager', 'iterate turn-key networks', '2022-04-01', NULL, 17, 0),
(54, NULL, 'Legal Assistant', 'embrace distributed vortals', '2022-07-18', NULL, 18, 1),
(55, NULL, 'Assistant Media Planner', 'drive front-end schemas', '2023-02-27', NULL, 12, 0),
(56, NULL, 'Safety Technician IV', 'engage value-added e-markets', '2024-03-29', NULL, 11, 0),
(57, NULL, 'Design Engineer', 'monetize revolutionary schemas', '2023-10-11', NULL, 19, 0),
(58, NULL, 'Media Manager IV', 'extend integrated eyeballs', '2023-11-21', NULL, 13, 1),
(59, NULL, 'Assistant Professor', 'incubate distributed vortals', '2023-07-22', NULL, 8, 1),
(60, NULL, 'Senior Sales Associate', 'generate dot-com deliverables', '2023-12-22', NULL, 5, 1),
(61, NULL, 'Assistant Media Planner', 'cultivate synergistic platforms', '2023-06-27', NULL, 13, 1),
(62, NULL, 'Dental Hygienist', 'unleash seamless e-services', '2023-07-31', NULL, 1, 0),
(63, NULL, 'Legal Assistant', 'strategize real-time channels', '2023-01-10', NULL, 4, 0),
(64, NULL, 'VP Sales', 'matrix collaborative metrics', '2023-03-08', NULL, 6, 1),
(65, NULL, 'Civil Engineer', 'mesh back-end communities', '2024-03-24', NULL, 18, 0),
(66, NULL, 'Speech Pathologist', 'unleash vertical web services', '2023-02-04', NULL, 12, 1),
(67, NULL, 'Financial Advisor', 'innovate vertical convergence', '2024-05-28', '2024-05-11', 18, 1),
(68, NULL, 'Recruiting Manager', 'synergize open-source action-items', '2023-08-27', NULL, 19, 0),
(69, NULL, 'Senior Developer', 'integrate transparent synergies', '2022-06-17', NULL, 9, 0),
(70, NULL, 'Safety Technician III', 'enhance sticky web-readiness', '2023-02-15', NULL, 11, 1),
(71, NULL, 'Engineer I', 'cultivate intuitive mindshare', '2023-04-23', NULL, 1, 1),
(72, NULL, 'Tax Accountant', 'productize integrated e-tailers', '2023-09-04', '2023-11-30', 18, 0),
(73, NULL, 'Developer IV', 'optimize e-business platforms', '2022-05-20', NULL, 1, 0),
(74, NULL, 'Senior Cost Accountant', 'architect distributed initiatives', '2023-12-27', NULL, 1, 0),
(75, NULL, 'Internal Auditor', 'syndicate robust systems', '2022-08-05', NULL, 8, 0),
(76, NULL, 'Research Nurse', 'repurpose 24/365 e-markets', '2023-02-14', NULL, 12, 1),
(77, NULL, 'Marketing Manager', 'optimize extensible web services', '2023-03-28', NULL, 13, 0),
(78, NULL, 'VP Product Management', 'grow wireless deliverables', '2022-07-11', '2023-05-31', 1, 0),
(79, NULL, 'Senior Quality Engineer', 'unleash synergistic bandwidth', '2022-04-21', NULL, 19, 0),
(80, NULL, 'Health Coach I', 'facilitate extensible interfaces', '2023-11-15', NULL, 1, 1),
(81, NULL, 'Chemical Engineer', 'redefine seamless web-readiness', '2023-07-10', NULL, 20, 1),
(83, NULL, 'Data Coordiator', 'iterate frictionless initiatives', '2022-07-01', NULL, 5, 1),
(84, NULL, 'VP Product Management', 'engineer magnetic solutions', '2023-06-04', NULL, 13, 0),
(85, NULL, 'Human Resources Manager', 'orchestrate dot-com schemas', '2023-03-16', '2023-01-30', 12, 1),
(86, NULL, 'Staff Accountant II', 'monetize viral architectures', '2023-11-08', NULL, 19, 1),
(87, NULL, 'Sales Associate', 'e-enable B2B experiences', '2023-03-03', '2022-02-23', 7, 1),
(88, NULL, 'Technical Writer', 'revolutionize bricks-and-clicks technologies', '2022-09-05', NULL, 16, 0),
(89, NULL, 'Statistician II', 'deploy next-generation e-tailers', '2022-11-02', NULL, 15, 0),
(90, NULL, 'Computer Systems Analyst III', 'innovate enterprise e-business', '2023-07-16', '2023-01-24', 14, 1),
(91, NULL, 'Nuclear Power Engineer', 'synergize frictionless eyeballs', '2022-06-23', NULL, 14, 0),
(92, NULL, 'Media Manager I', 'disintermediate B2B vortals', '2023-03-04', NULL, 13, 1),
(93, NULL, 'Recruiter', 'target viral e-commerce', '2023-03-18', NULL, 19, 1),
(94, NULL, 'Senior Cost Accountant', 'maximize value-added communities', '2022-12-09', NULL, 2, 1),
(95, NULL, 'Civil Engineer', 'maximize killer infrastructures', '2022-10-06', '2023-02-27', 15, 0),
(96, NULL, 'Registered Nurse', 'envisioneer distributed interfaces', '2024-01-28', NULL, 11, 1),
(97, NULL, 'Pharmacist', 'transform integrated platforms', '2022-09-27', NULL, 10, 0),
(98, NULL, 'Senior Editor', 'aggregate wireless communities', '2023-11-09', NULL, 20, 0),
(99, NULL, 'Physical Therapy Assistant', 'evolve proactive niches', '2023-07-29', NULL, 20, 0),
(100, NULL, 'Dental Hygienist', 'embrace bleeding-edge platforms', '2023-06-12', NULL, 2, 0),
(101, NULL, 'Research Assistant II', 'unleash seamless interfaces', '2023-12-18', NULL, 10, 1),
(102, NULL, 'Sales Representative', 'brand distributed initiatives', '2023-01-06', NULL, 6, 1),
(103, NULL, 'Clinical Specialist', 'synthesize ubiquitous metrics', '2023-06-22', '2024-02-23', 14, 1),
(104, NULL, 'Software Test Engineer I', 'reinvent dot-com e-markets', '2023-03-26', '2023-01-14', 5, 0),
(105, 3, 'Account Executive', 'target synergistic partnerships', '2022-03-18', NULL, 20, 0),
(106, NULL, 'Staff Scientist', 'optimize interactive e-services', '2023-09-04', NULL, 17, 0),
(107, NULL, 'Electrical Engineer', 'innovate holistic relationships', '2023-01-25', '2022-09-20', 15, 1),
(108, NULL, 'Executive Secretary', 'incentivize best-of-breed ROI', '2023-11-01', NULL, 17, 1),
(109, NULL, 'Recruiter', 'synergize seamless architectures', '2023-08-27', NULL, 14, 1),
(110, NULL, 'Chief Design Engineer', 'e-enable cutting-edge functionalities', '2022-11-26', NULL, 8, 0),
(111, NULL, 'Marketing Assistant', 'exploit frictionless e-tailers', '2023-07-01', NULL, 2, 0),
(112, NULL, 'Environmental Tech', 'deliver bricks-and-clicks communities', '2024-02-24', NULL, 19, 1),
(113, NULL, 'Help Desk Operator', 'mesh magnetic web-readiness', '2023-03-11', '2024-01-05', 9, 1),
(114, NULL, 'Quality Control Specialist', 'evolve back-end synergies', '2023-11-18', NULL, 12, 0),
(115, NULL, 'Senior Quality Engineer', 'visualize cutting-edge relationships', '2022-06-13', '2022-06-29', 19, 0),
(116, NULL, 'Cost Accountant', 'integrate visionary infomediaries', '2022-06-30', '2024-04-06', 9, 1),
(117, NULL, 'Civil Engineer', 'orchestrate B2C content', '2024-05-27', NULL, 13, 1),
(118, NULL, 'Senior Cost Accountant', 'seize end-to-end e-markets', '2023-02-05', NULL, 15, 1),
(119, NULL, 'VP Marketing', 'harness distributed niches', '2023-09-08', NULL, 10, 0),
(120, NULL, 'VP Sales', 'strategize distributed functionalities', '2023-06-27', NULL, 14, 0),
(121, NULL, 'Human Resources Manager', 'scale extensible web-readiness', '2024-05-02', NULL, 9, 0),
(122, NULL, 'Information Systems Manager', 'matrix viral channels', '2023-07-22', '2023-09-16', 6, 0),
(123, NULL, 'General Manager', 'integrate real-time action-items', '2022-02-28', '2023-03-20', 7, 0),
(124, NULL, 'Senior Financial Analyst', 'extend dot-com channels', '2023-10-21', NULL, 13, 0),
(125, NULL, 'Senior Financial Analyst', 'monetize B2B web-readiness', '2024-01-11', '2023-06-13', 6, 1),
(126, NULL, 'Compensation Analyst', 'integrate scalable eyeballs', '2022-04-28', NULL, 19, 1),
(127, NULL, 'Structural Analysis Engineer', 'benchmark magnetic deliverables', '2022-11-02', '2023-09-27', 3, 1),
(128, NULL, 'Programmer II', 'implement world-class synergies', '2022-11-24', NULL, 7, 0),
(129, NULL, 'Staff Scientist', 'drive 24/7 portals', '2024-05-29', NULL, 8, 1),
(130, NULL, 'Nurse Practicioner', 'scale interactive ROI', '2023-09-22', '2022-03-09', 20, 1),
(131, NULL, 'VP Quality Control', 'grow customized functionalities', '2023-01-24', NULL, 7, 1),
(132, NULL, 'Senior Quality Engineer', 'leverage scalable partnerships', '2023-11-08', '2023-02-05', 15, 1),
(133, NULL, 'Marketing Manager', 'iterate turn-key networks', '2022-04-01', NULL, 17, 0),
(134, NULL, 'Legal Assistant', 'embrace distributed vortals', '2022-07-18', NULL, 18, 1),
(135, NULL, 'Assistant Media Organizer', 'drive front-end schemas', '2023-02-27', '2023-03-01', 12, 0),
(136, NULL, 'Safety Technician IV', 'engage value-added e-markets', '2024-03-29', NULL, 11, 0),
(137, NULL, 'Design Engineer', 'monetize revolutionary schemas', '2023-10-11', NULL, 19, 0),
(138, NULL, 'Media Manager IV', 'extend integrated eyeballs', '2023-11-21', NULL, 13, 1),
(140, NULL, 'Senior Sales Associate', 'generate dot-com deliverables', '2023-12-22', NULL, 5, 1),
(141, NULL, 'Assistant Media Planner', 'cultivate synergistic platforms', '2023-06-27', NULL, 13, 1),
(142, NULL, 'Dental Hygienist', 'unleash seamless e-services', '2023-07-31', NULL, 1, 0),
(143, NULL, 'Legal Assistant', 'strategize real-time channels', '2023-01-10', NULL, 4, 0),
(144, NULL, 'VP Sales', 'matrix collaborative metrics', '2023-03-08', NULL, 6, 1),
(145, NULL, 'Civil Engineer', 'mesh back-end communities', '2024-03-24', NULL, 18, 0),
(146, NULL, 'Speech Pathologist', 'unleash vertical web services', '2023-02-04', NULL, 12, 1),
(147, NULL, 'Financial Advisor', 'innovate vertical convergence', '2024-05-28', '2024-05-11', 18, 1),
(148, NULL, 'Recruiting Manager', 'synergize open-source action-items', '2023-08-27', NULL, 19, 0),
(149, NULL, 'Senior Developer', 'integrate transparent synergies', '2022-06-17', NULL, 9, 0),
(150, NULL, 'Safety Technician III', 'enhance sticky web-readiness', '2023-02-15', NULL, 11, 1),
(151, NULL, 'Engineer I', 'cultivate intuitive mindshare', '2023-04-23', NULL, 1, 1),
(152, NULL, 'Tax Accountant', 'productize integrated e-tailers', '2023-09-04', '2023-11-30', 18, 0),
(153, NULL, 'Developer IV', 'optimize e-business platforms', '2022-05-20', NULL, 1, 0),
(154, NULL, 'Senior Cost Accountant', 'architect distributed initiatives', '2023-12-27', NULL, 1, 0),
(155, NULL, 'Internal Auditor', 'syndicate robust systems', '2022-08-05', NULL, 8, 0),
(156, NULL, 'Research Nurse', 'repurpose 24/365 e-markets', '2023-02-14', NULL, 12, 1),
(157, NULL, 'Marketing Manager', 'optimize extensible web services', '2023-03-28', NULL, 13, 0),
(158, NULL, 'VP Product Management', 'grow wireless deliverables', '2022-07-11', '2023-05-31', 1, 0),
(159, NULL, 'Senior Quality Engineer', 'unleash synergistic bandwidth', '2022-04-21', NULL, 19, 0),
(160, NULL, 'Health Coach I', 'facilitate extensible interfaces', '2023-11-15', NULL, 1, 1),
(161, NULL, 'Testkkurs', 'Test Beschreibung', '2023-03-20', NULL, 12, 1),
(165, NULL, 'Neuer Kurs', 'Hallo neuer Kurs', '2023-03-30', '2023-03-12', 12, 1),
(167, NULL, 'Tom Test', 'Jipie, eine Beschreibung', '2023-03-18', NULL, 8, 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `rooms`
--

CREATE TABLE `rooms` (
  `room_id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `seats` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `rooms`
--

INSERT INTO `rooms` (`room_id`, `title`, `description`, `seats`) VALUES
(1, 'Media-001', 'Media Courses, Adobe etc.', 12),
(2, 'Dev-002', 'Programming Courses', 20),
(3, 'DEV-003', 'Visual Studio', 10);

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`course_id`);

--
-- Indizes für die Tabelle `rooms`
--
ALTER TABLE `rooms`
  ADD PRIMARY KEY (`room_id`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `courses`
--
ALTER TABLE `courses`
  MODIFY `course_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=168;

--
-- AUTO_INCREMENT für Tabelle `rooms`
--
ALTER TABLE `rooms`
  MODIFY `room_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
