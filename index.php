<?php
declare(strict_types=1);
session_start();
require "vendor/autoload.php";
\Tracy\Debugger::enable();

// Fatfree Objekt erzeugen
$f3 = \Base::instance();

// Routing testen
// Wenn eine GET Anfrage an die Homepage kommt, führe folgende Funktion aus.
// Simple Methode um das Routing zu testen
// $f3->route('GET /', function($f3, $params){
// 	echo 'Jipipe';
// });

// $f3->route('GET /contact', function($f3, $params){
// 	echo 'Kontakt';
// });

/*
	Rufe im Namespace Controller die Methode IndexController->index auf. Sie kümmert
	sich um die Ausgabe unserer Seite.
*/
$f3->route('GET @home: /', 		'\Controller\IndexController->index');
$f3->route('GET /imprint', 		'\Controller\IndexController->imprint');
$f3->route('GET /contact', 		'\Controller\IndexController->contact');
$f3->route('GET /course/@cid', 	'\Controller\CourseController->course');

// CRUD Courses
$f3->route('GET @courseNewForm: /course/new', '\Controller\CourseController->createForm');
$f3->route('POST /course/new', '\Controller\CourseController->create');
// Standard GET Delete route
$f3->route('GET @deleteCourse: /course/delete/@cid', '\Controller\CourseController->delete');
// REST delete route
$f3->route('DELETE @deleteCourse: /course', '\Controller\CourseController->deleteREST');

// Update
$f3->route('GET @courseUpdateForm: /course/update/@cid', '\Controller\CourseController->updateForm');
$f3->route('POST /course/update/@cid', '\Controller\CourseController->update');


$f3->route('GET @trainerHome: /trainer', '\Controller\TrainerController->index');

// Login routes
$f3->route('GET @loginForm: /login', '\Controller\LoginController->loginForm');
$f3->route('POST /login', '\Controller\LoginController->login');
$f3->route('GET @logout: /logout', '\Controller\LoginController->logout');

// Backend - nur für angemeldete Trainer
$f3->route('GET @backend: /backend', '\Controller\BackendController->index');
// Fatfree starten
$f3->run();