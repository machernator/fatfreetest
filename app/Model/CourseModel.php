<?php
namespace Model;

class CourseModel extends Model {
	/**
	 * Alle Kurse auslesen
	 *
	 * @return void
	 */
	public function courses()
	{
		// prepare
		$stmt = $this->pdo->prepare('SELECT * FROM courses ORDER BY title ASC');
		// bind, hier nicht notwendig

		// ausführen
		$stmt->execute();
		// gib alle Zeilen zurück, erzeuge nur assoziatives array mit Spaltennamen als keys
		return $stmt->fetchAll(\PDO::FETCH_ASSOC);
	}

	public function course(int $id) : array
	{
		$row = [];
		$stmt = $this->pdo->prepare('SELECT * FROM courses WHERE course_id=:course_id');
		$stmt->bindParam(':course_id', $id);
		$stmt->execute();
		$row = $stmt->fetch(\PDO::FETCH_ASSOC);
		return $row;
	}

	/**
	 * Create new course
	 *
	 * @param string $title
	 * @param string $description
	 * @param string $date_start
	 * @param string $date_end
	 * @param integer $max_students
	 * @param integer $active
	 * @return boolean
	 */
	public function create(
		string $title,
		string $description,
		string $date_start,
		string $date_end,
		int $max_students,
		mixed $room_id,  // int oder null weil kein Pflichtfeld
		int $active
	) : bool
	{
		$sql = "INSERT INTO courses (title, description, date_start, date_end, max_students, room_id, active)
				VALUES (:title, :description, :date_start, :date_end, :max_students, :room_id, :active)";
		$stmt = $this->pdo->prepare($sql);
		// Leerstrings in null Werte umwandeln
		$date_end = $date_end === '' ? null : $date_end;

		$stmt->bindParam('title', $title);
		$stmt->bindParam('description', $description);
		$stmt->bindParam('date_start', $date_start);
		$stmt->bindParam('date_end', $date_end);
		$stmt->bindParam('max_students', $max_students);
		$stmt->bindParam('room_id', $room_id);
		$stmt->bindParam('active', $active);
		$stmt->execute();
		// TODO: try - catch verwenden, um true/false zurückzugeben
		return true;
	}

	/**
	 * Update course data
	 *
	 * @param string $title
	 * @param string $description
	 * @param string $date_start
	 * @param string $date_end
	 * @param integer $max_students
	 * @param integer $active
	 * @return boolean
	 */
	public function update(
		int 	$course_id,
		string 	$title,
		string 	$description,
		string 	$date_start,
		string 	$date_end,
		int 	$max_students,
		mixed $room_id,  // int oder null weil kein Pflichtfeld
		int 	$active
	) : bool
	{
		$sql = "UPDATE courses
				SET title=:title,
					description=:description,
					date_start=:date_start,
					date_end=:date_end,
					max_students=:max_students,
					room_id=:room_id,
					active=:active
				WHERE course_id=:course_id";
		$stmt = $this->pdo->prepare($sql);
		// Leerstrings in null Werte umwandeln
		$date_end = $date_end === '' ? null : $date_end;
		$stmt->bindParam('course_id', $course_id);
		$stmt->bindParam('title', $title);
		$stmt->bindParam('description', $description);
		$stmt->bindParam('date_start', $date_start);
		$stmt->bindParam('date_end', $date_end);
		$stmt->bindParam('max_students', $max_students);
		$stmt->bindParam('room_id', $room_id);
		$stmt->bindParam('active', $active);
		$stmt->execute();
		// TODO: try - catch verwenden, um true/false zurückzugeben
		return true;
	}

	/**
	 * Delete a course
	 *
	 * @param integer $cid
	 * @return void
	 */
	public function delete(int $cid) {
		$stmt = $this->pdo->prepare('DELETE FROM courses WHERE course_id=:course_id');
		$stmt->bindParam('course_id', $cid);
		$stmt->execute();
		// TODO: Fehler abfangen, false bei Fehler zurückgeben
		return true;
	}
}