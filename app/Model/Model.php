<?php
namespace Model;

abstract class Model {
	protected $pdo ;
	public function __construct()
	{
		try {
			// BAD PRACTICE, sollte in ini/env/yaml ausgelagert werden.
			$this->pdo = new \PDO("mysql:host=localhost;dbname=institut", 'root', '');
		} catch(\Exception $e) {
			dumpe($e);
		}
	}
}