<?php
namespace Model;

class UserModel extends Model {
	public function loginUser(string $email) {
		$sql = 'SELECT * FROM users WHERE email = :email';
		$statement = $this->pdo->prepare($sql);
		$statement->execute(['email' => $email]);
		$user = $statement->fetch();
		return $user;
	}
}