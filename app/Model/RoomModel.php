<?php
namespace Model;

class RoomModel extends Model
{
	/**
	 * Select all rooms
	 */
	public function rooms():array
	{
		$stmt = $this->pdo->prepare('SELECT * FROM rooms ORDER BY title ASC');
		$stmt->execute();
		return $stmt->fetchAll(\PDO::FETCH_ASSOC);
	}
}