<?php
namespace Model;

class TrainerModel extends Model {
	public function trainers()
	{
		# get all trainers
	}

	public function trainer(int $id) : array
	{
		# get trainer by id
	}

	public function create(
		// params
	) : bool
	{
		# create new trainer
	}

	public function update(
		// params
	) : bool
	{
		# update trainer
	}

	public function delete(int $id) : bool
	{
		# delete trainer
	}
}