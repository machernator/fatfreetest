<?php

namespace Controller;
/*
TODO: Noch Probleme mit Appstatus und dem Löschen der Session Variablen in den Formularen
*/

class CourseController
{
	/**
	 * Alle Kurse auslesen
	 *
	 * @param object $f3
	 * @param array $params
	 * @return void
	 */
	public function course(\Base $f3, array $params)
	{
		$cid = $params['cid'] ?? null;

		if (!filter_var($cid, FILTER_VALIDATE_INT)) {
			// manipulierte URL, ignorieren und auf home weiterleiten
			$f3->reroute('@home');
			return;
		}

		$cm = new \Model\CourseModel();
		$course = $cm->course($cid);
		$f3->set('course', $course);

		$f3->set('siteTitle', 'Kurs - ' . $course['title']);
		// Mit $f3->get könnten wir die Variable nun aufrufen
		// echo $f3->get('siteTitle');
		$f3->set('mainHeading',  $course['title']);
		$f3->set('content', 'course.html');

		echo \Template::instance()->render('views/index.html');
	}

	/**
	 * Create course form darstellen
	 *
	 * @param object $f3
	 * @param array $params
	 * @return void
	 */
	public function createForm($f3, $params)
	{
		$f3->set('siteTitle', 'Kurs - Neuer Kurs');
		$f3->set('mainHeading',  'Neuer Kurs');
		$f3->set('content', 'create-course.html');

		// handling potentieller errors/values aus abgesendeten Formular
		$f3->set('fieldValues', $_SESSION['newCourseValues'] ?? []);
		$f3->set('fieldErrors', $_SESSION['newCourseErrors'] ?? []);
		$f3->set('appStatus', $_SESSION['appStatus'] ?? []);

		// zuvor gesetzte errors/values in der session löschen
		unset($_SESSION['newCourseValues']);
		unset($_SESSION['newCourseErrors']);
		unset($_SESSION['appStatus']);

		$rm = new \Model\RoomModel();
		$rooms = $rm->rooms();
		$f3->set('rooms', $rooms);

		echo \Template::instance()->render('views/index.html');
	}

	/**
	 * Form post, validate data, display messages
	 *
	 * @param object $f3
	 * @param array $params
	 * @return void
	 */
	public function create(object $f3, array $params): void
	{
		// BEST PRACTICE: auch die Validierung sollte im Model geschehen
		// Validieren
		$gump = new \GUMP('de');

		$gump->validation_rules([
			'title' => 				'required|max_len,255',
			'description' =>		'max_len,2000',
			'date_start' => 		'required|date',
			'date_end' => 			'date',
			'max_students' =>		'required|integer|min_numeric,1',
			'active' => 			'boolean',
			'room_id' => 			'integer'
		]);

		$gump->filter_rules([
			'title' => 				'trim|sanitize_string',
			'description' =>		'trim|sanitize_string',
			'date_start' => 		'trim',
			'date_end' => 			'trim',
			'max_students' =>		'trim',
			'active' => 			'trim',
			'room_id' => 			'trim'
		]);
		$data = $gump->run($_POST);

		// Wenn nicht gültig, Fehlermeldungen und abgeschickte Daten zeigen
		if ($gump->errors()) {
			// Errors und abgeschickte values in Session zwischenspeichern.
			$_SESSION['newCourseValues'] = $_POST;
			$_SESSION['newCourseErrors'] = $gump->get_errors_array();
			$_SESSION['appStatus']['error'] = 'Das Formular hat fehlerhafte Eingaben.';
			// redirect nach neuer course
			$f3->reroute('@courseNewForm');
		}
		// Wenn gültig, speichern, Erfolgsmeldung
		else {
			$cm = new \Model\CourseModel();
			// named arguments
			$cm->create(
				description: $data['description'],
				title: $data['title'],
				date_end: $data['date_end'],
				date_start: $data['date_start'],
				active: $data['active'],
				max_students: $data['max_students'],
				/*
					Ein nicht Pflichtfeld sollte eigentlich eine integer erhalten. Da sie
					aber auch Leer sein kann, wandeln wir den Leerstring aus dem
					Formular in den Wert null um.
				*/
				room_id: $data['room_id'] === '' ? null : $data['room_id']
			);

			// Appstatus setzen
			$_SESSION['appStatus']['success'] = 'Daten erfolgreich gespeichert';
			// redirect nach neuer course
			$f3->reroute('@courseNewForm');
		}
	}

	/**
	 * Update Form for a course defined by $params['cid']
	 *
	 * @param object $f3
	 * @param array $params
	 * @return void
	 */
	public function updateForm(object $f3, array $params)
	{
		// TODO: einiger Code ist gleich wie in createForm. Wie kann das verhindert werden?
		$cid = $params['cid'];
		// cid validieren auf Integer
		if (!filter_var($cid, FILTER_VALIDATE_INT)) {
			// manipulierte URL, ignorieren und auf home weiterleiten
			$f3->reroute('@home');
			return;
		}
		// Daten für den Kurs aus dem Coursemodel auslesen
		$cm = new \Model\CourseModel();
		$course = $cm->course($cid);

		// Daten als f3 Variablen für die view setzen
		/*
			Wenn Fehler auftraten, wollen wir die Werte aus dem abgeschickten
			Formular noch einmal zeigen. Traten keine Fehler auf, schreiben
			wir die Werte aus der DB in das Formular!
		*/
		$f3->set('fieldValues', $_SESSION['updateCourseValues'] ?? $course);

		$f3->set('fieldErrors', $_SESSION['updateCourseErrors'] ?? []);
		$f3->set('appStatus', $_SESSION['appStatus'] ?? []);
		$f3->set('siteTitle', 'Kurs - Update ' . $course['title']);
		$f3->set('mainHeading',  'Update ' . $course['title']);
		$f3->set('content', 'update-course.html');

		// zuvor gesetzte errors/values in der session löschen
		unset($_SESSION['updateCourseValues']);
		unset($_SESSION['updateCourseErrors']);
		unset($_SESSION['appStatus']);

		$rm = new \Model\RoomModel();
		$rooms = $rm->rooms();
		$f3->set('rooms', $rooms);

		// Template aufrufen und anzeigen
		echo \Template::instance()->render('views/index.html');
	}

	/**
	 * Validate and save course update data
	 *
	 * @param object $f3
	 * @param array $params
	 * @return void
	 */
	public function update(object $f3, array $params)
	{
		// Validieren
		$gump = new \GUMP('de');

		$gump->validation_rules([
			'course_id' => 			'required|integer',
			'title' => 				'required|max_len,255',
			'description' =>		'max_len,2000',
			'date_start' => 		'required|date',
			'date_end' => 			'date',
			'max_students' =>		'required|integer|min_numeric,1',
			'room_id' => 			'integer',
			'active' => 			'boolean'
		]);

		$gump->filter_rules([
			'course_id' =>			'trim',
			'title' => 				'trim|sanitize_string',
			'description' =>		'trim|sanitize_string',
			'date_start' => 		'trim',
			'date_end' => 			'trim',
			'max_students' =>		'trim',
			'room_id' =>			'trim',
			'active' => 			'trim'
		]);
		$data = $gump->run($_POST);

		// Wenn nicht gültig, Fehlermeldungen und abgeschickte Daten zeigen
		if ($gump->errors()) {
			// Errors und abgeschickte values in Session zwischenspeichern.
			$_SESSION['updateCourseValues'] = $_POST;
			$_SESSION['updateCourseErrors'] = $gump->get_errors_array();
			$_SESSION['appStatus']['error'] = 'Das Formular hat fehlerhafte Eingaben.';
			// redirect nach neuer course
			$f3->reroute('@courseUpdateForm');
		}
		// Wenn gültig, speichern, Erfolgsmeldung
		else {
			$cm = new \Model\CourseModel();
			// named arguments
			$cm->update(
				course_id: $data['course_id'],
				description: $data['description'],
				title: $data['title'],
				date_end: $data['date_end'],
				date_start: $data['date_start'],
				active: $data['active'],
				room_id: $data['room_id'] === '' ? null : $data['room_id'],
				max_students: $data['max_students']
			);

			// Appstatus setzen
			$_SESSION['appStatus']['success'] = 'Daten erfolgreich gespeichert';
			// redirect nach neuer course
			$f3->reroute('@courseUpdateForm');
		}
	}

	/**
	 * Delete a course
	 *
	 * @param object $f3
	 * @param array $params
	 * @return void
	 */
	public function delete(object $f3, array $params)
	{
		$cid = $params['cid'];
		// cid validieren auf Integer
		if (!filter_var($cid, FILTER_VALIDATE_INT)) {
			// manipulierte URL, ignorieren und auf home weiterleiten
			$f3->reroute('@home');
			return;
		}

		$cm = new \Model\CourseModel();
		$isDeleted = $cm->delete($cid);

		if ($isDeleted) {
			$_SESSION['appStatus']['success'] = 'Datensatz wurde erfolgreich gelöscht.';
		} else {
			$_SESSION['appStatus']['error'] = 'Datensatz konnte nicht gelöscht werden.';
		}

		$f3->reroute('@home');

		/*
			Beispiel für RESTFUL API
			file_get_contents liest alle Inhalte aus einem File aus
			die einzige Methode, um auf über PUT, PATCH, DELETE ... Daten
			zuzugreifen (http body)
			parse_str wandelt uns die Daten aus PUT, PATCH, DELETE in
			ein assoziatives Array um.
			$data = [];
			parse_str(file_get_contents("php://input"), $data);


			header('Content-type: application/json');
			echo '{"message": "Successfully deleted: ' . $data['cid'] . '"}';
		*/
	}

	public function deleteREST(object $f3, array $params)
	{
		header('Content-type: application/json');
		// Body des Request auslesen
		$data = file_get_contents("php://input");
		$contents = [];
		// name/value pairs aus dem Request body in $contents speichern (Array)
		parse_str($data, $contents);
		$cid = $contents['cid'] ?? null;

		// Auf int validieren
		if (!filter_var($cid, FILTER_VALIDATE_INT)) {
			echo '{"error": "invalid request"}';
			return;
		}

		$cm = new \Model\CourseModel();
		$res = $cm->delete($cid);

		if(!$res) {
			echo '{"error": "could not delete dataset"}';
			return;
		}
		echo '{"error": null, "message": "successfully deleted dataset"}';
	}

}