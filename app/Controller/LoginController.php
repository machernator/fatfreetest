<?php
namespace Controller;

class LoginController {
	/**
	 * Login Formular anzeigen
	 *
	 * @param object $f3
	 * @param array $params
	 * @return void
	 */
	public function loginForm(\Base $f3, array $params):void
	{
		$f3->set('siteTitle', 'Login');
		$f3->set('mainHeading',  'Login');
		$f3->set('content', 'login.html');

		$f3->set('appStatus', $_SESSION['appStatus'] ?? []);
		unset($_SESSION['appStatus']);

		echo \Template::instance()->render('views/index.html');
	}

	/**
	 * Login ausführen
	 * 		- User
	 * 			- existiert
	 * 			- Passwort stimmt überein
	 * 				- Session starten
	 * 				- Weiterleitung auf Seite X
	 * 			- Passwort stimmt nicht überein
	 * 				- Fehlermeldung
	 * 				- Weiterleitung auf Login
	 * 		- User existiert nicht
	 * 			- Fehlermeldung
	 * 				- Weiterleitung auf Login
	 *
	 * @param object $f3
	 * 		- Framework Objekt
	 * @param array $params
	 * @return void
	 *
	 */
	public function login(\Base $f3, array $params):void
	{
		$um = new \Model\UserModel();
		// TODO: validieren, ob E-Mail-Adresse gültig ist
		$user = $um->loginUser(trim($_POST['email']));

		if ($user === false) {
			// User existiert nicht
			$_SESSION['appStatus']['error'] = 'Ungültiges Login.';
			$f3->reroute('@loginForm');
			return;
		}
		else {
			// User existiert
			// Passwort prüfen
			$isValid = password_verify(trim($_POST['password']), $user['password']);
			if($isValid) {
				// Passwort stimmt überein
				$_SESSION['appStatus']['success'] = 'Login erfolgreich.';
				$_SESSION['userName'] = $user['user_name'];
				$_SESSION['isLoggedIn'] = true;
				$f3->reroute('@backend');
				return;
			}
			else {
				// Passwort stimmt nicht überein
				$_SESSION['appStatus']['error'] = 'Ungültiges Login.';
				$f3->reroute('@loginForm');
				return;
			}
		}
	}
	
	public function logout(\Base $f3, array $params): void
	{
		session_destroy();
		$f3->reroute('@loginForm');
	}
}