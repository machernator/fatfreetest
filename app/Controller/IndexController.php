<?php
namespace Controller;

/*
	Eine Controller Klasse gruppiert zusammengehörige Methoden, die über routes aufgerufen werden sollen.

	z. B. Alle Routes, die mit Kunden zu tun haben, werden in einer Klasse CustomerController zusammengefasst.
	Alles Routes, die mit dem Shop zu tun haben, werden in einer Klasse ShopController zusammengefasst.
	...
*/
class IndexController {
	/*
		Jede Methode, die von einer F3 route aufgerufen wird, erhält 2 Parameter:
		$f3 -> Fatfree Objekt
		$params -> hier können dynamische Parameter in der URL mitgegeben werden.
	*/
	public function index($f3, $params)
	{
		/*
			Jede Variable, die wir mit f3->set setzen, steht uns automatisch überall, wo wir Zugriff
			auf das F3 Objekt haben, UND IN DEN TEMPLATES zur Verfügung.
		*/
		$f3->set('siteTitle', 'Welcome Home');
		// Mit $f3->get könnten wir die Variable nun aufrufen
		// echo $f3->get('siteTitle');
		$f3->set('mainHeading', 'Welcome to our wonderful page!');
		$f3->set('content', 'home.html');

		$cm = new \Model\CourseModel();
		$courses = $cm->courses();
		// Variable fürs Template erstellen
		$f3->set('courses', $courses);

		// handling potentieller errors/values aus abgesendeten Formular
		$f3->set('fieldValues', $_SESSION['newCourseValues'] ?? []);
		$f3->set('fieldErrors', $_SESSION['newCourseErrors'] ?? []);
		$f3->set('appStatus', $_SESSION['appStatus'] ?? []);
		// zuvor gesetzte errors/values in der session löschen
		unset($_SESSION['newCourseValues']);
		unset($_SESSION['newCourseErrors']);
		unset($_SESSION['appStatus']);

		echo \Template::instance()->render('views/index.html');
	}

	public function imprint($f3, $params)
	{
		$f3->set('siteTitle', 'Impressum');
		$f3->set('mainHeading', 'Impressum unserer Seite');
		$f3->set('content', 'imprint.html');
		echo \Template::instance()->render('views/index.html');
	}

	public function contact($f3, $params)
	{
		$f3->set('siteTitle', 'Contact');
		$f3->set('mainHeading', 'Kontakt');
		$f3->set('content', 'contact.html');
		echo \Template::instance()->render('views/index.html');
	}

}