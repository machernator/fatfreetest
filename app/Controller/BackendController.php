<?php
namespace Controller;

class BackendController {
	/**
	 * Prüfen, ob User eingeloggt ist
	 * 		- wenn nicht, dann auf Login weiterleiten
	 *
	 * beforeRoute ist eine fatfree Methode, die vor jeder Route ausgeführt wird.
	 *
	 * @param object $f3
	 * @param array $params
	 * @return void
	 */
	public function beforeRoute(\Base $f3, array $params):void
	{
		// Prüfen, ob User eingeloggt ist
		$isLoggedIn = $_SESSION['isLoggedIn'] ?? false;
		if ($isLoggedIn !== true) {
			// User ist nicht eingeloggt
			// Weiterleitung auf Login
			$f3->reroute('@loginForm');
		}

		$f3->set('userName', $_SESSION['userName']);
	}

	/**
	 * Backend - Startseite des Backend, das nur eingeloggten Trainern angezeigt wird.
	 *
	 * @param object $f3
	 * @param array $params
	 * @return void
	 */
	public function index(\Base $f3, array $params):void
	{
		$f3->set('siteTitle', 'Backend');
		$f3->set('mainHeading',  'Backend');
		$f3->set('content', 'backend.html');

		$f3->set('appStatus', $_SESSION['appStatus'] ?? []);
		unset($_SESSION['appStatus']);

		echo \Template::instance()->render('views/index.html');
	}
}