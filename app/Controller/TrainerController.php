<?php
namespace Controller;

class TrainerController
{
	public function index(\Base $f3, array $params):void
	{
		$f3->set('siteTitle', 'Trainer');
		$f3->set('mainHeading',  'Trainer');
		$f3->set('content', 'trainer.html');

		echo \Template::instance()->render('views/index.html');
	}
}
